import React from "react";
import {Card} from "react-bootstrap";
import ProgressBar from 'react-bootstrap/ProgressBar';
import "./VotingCard.css"
const VotingCard = (props) => {
    let {artist, totalVotes, hasVoted } = props;
    let votesPercetage = Math.round((artist.votes/totalVotes) * 100);

    return (
        <Card className="voting_card" >
            <Card.Img variant="top" src={`/assets/images/${artist.logo}`} className="artist_image"/>
            <Card.Body>
                <Card.Title className="artists_name">{artist.name}</Card.Title>
            </Card.Body>
           {hasVoted ? <Card.Footer><ProgressBar variant="danger" now={votesPercetage} label={`${votesPercetage}%`}/></Card.Footer>: "" } 
        </Card>
    )
}

export default VotingCard;