/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { Container, Row, Col, Toast } from "react-bootstrap";
import { VotingCard } from "./components/";
import artistsData from "./data/bands.json";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";

const App = () => {
  const [artists, setArtists] = useState([]);
  const [totalVotes, setTotalVotes] = useState(0);
  const [hasVoted, setHasVoted] = useState(false);

  useEffect(() => {
    setArtists(artistsData);
    getTotal();
  }, []);

  const incrementVotes = (artistId) => {
    artists = artists.map((artist) => {
      if (artist._id === artistId) {
        artist.votes += 1;
      }
      return artist;
    });
    setArtists(artists);
  };

  const getTotal = () => {
    setTotalVotes(
      artistsData.reduce((total, artist) => artist.votes + total, 0)
    );
  };

  return (
    <Container className="app">
      <Row className="poll_options">
        {artists.map((artist) => {
          return (
            <Col md={2}>
              <a
                onClick={() => {
                  setHasVoted(true);
                  incrementVotes(artist._id);
                }}
              >
                <VotingCard
                  artist={artist}
                  totalVotes={totalVotes}
                  hasVoted={hasVoted}
                />
              </a>
            </Col>
          );
        })}
      </Row>
    </Container>
  );
};

export default App;
